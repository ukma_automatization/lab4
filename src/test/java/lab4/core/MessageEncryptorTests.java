package lab4.core;

import org.junit.Assert;
import org.junit.Test;

public class MessageEncryptorTests {

	@Test
	public void enctryptTwoMessages_whenMessagesEqual_thenByteArraysShouldBeEqual() throws Exception {
		Message msg1 = new Message(1, "Hello");
		Message msg2 = new Message(1, "Hello");
		var arr1 = MessageEncryptor.encryptMessage(msg1);
		var arr2 = MessageEncryptor.encryptMessage(msg2);
		Assert.assertArrayEquals(arr1, arr2);
	}
	
}

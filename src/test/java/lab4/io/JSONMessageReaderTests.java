package lab4.io;

import org.junit.Assert;
import org.junit.Test;

import lab4.core.Message;

public class JSONMessageReaderTests {

	@Test
	public void readMessage_whenJSONFile_thenGetMessageObject() throws Exception {
		JSONMessageReader reader = new JSONMessageReader();
		Message msg = reader.readJSONMessage();
		Assert.assertEquals(msg.getUserId(), 10);
		Assert.assertEquals(msg.getMessage(), "Hi, there!");
	}
	
}

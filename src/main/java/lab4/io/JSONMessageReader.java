package lab4.io;

import java.io.FileReader;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import lab4.core.Message;

public class JSONMessageReader {

	private static final String FILENAME = "message.json";
	
	public JSONMessageReader() {}
	
	public Message readJSONMessage() throws Exception {
		JSONParser parser = new JSONParser();
		URL url = getClass().getClassLoader().getResource(FILENAME);
		Path path = Paths.get(url.toURI());
		FileReader reader = new FileReader(path.toString());
		Object obj = parser.parse(reader);
        JSONObject jsonMsg = (JSONObject) obj;
        return new Message(Integer.parseInt((String)jsonMsg.get("userId")), (String)jsonMsg.get("message"));
	}
	
}

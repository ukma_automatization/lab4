package lab4.coreUtils;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class MyCipher {
	
	private SecretKey secretKey;

	private Cipher cipher;
	
    private static final String ENCRYPTION_KEY_STRING =  "abfgdhjutgfHHgGfaKLiOOwqyrNMVCBl";
    
    private static final String ALGORITHM = "AES";
    
    public MyCipher() {
    	try {
    		this.cipher = Cipher.getInstance(ALGORITHM);
    		this.secretKey = new SecretKeySpec(ENCRYPTION_KEY_STRING.getBytes(), ALGORITHM);
    	} catch(NoSuchAlgorithmException | NoSuchPaddingException e) {
    		e.printStackTrace();
    	}
    }
    
    public byte[] encrypt(String str) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
    	this.cipher.init(Cipher.ENCRYPT_MODE, secretKey);
    	return cipher.doFinal(str.getBytes());
    }
    
}


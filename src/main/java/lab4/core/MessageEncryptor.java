package lab4.core;

import java.nio.ByteBuffer;

import lab4.coreUtils.MyCipher;

public class MessageEncryptor {

	private static final MyCipher cipher = new MyCipher();
	
	public static byte[] encryptMessage(Message msg) throws Exception {
		var encryptedMsg = cipher.encrypt(msg.getMessage());
		ByteBuffer bytes = ByteBuffer.allocate(Short.BYTES + 2 * Integer.BYTES + encryptedMsg.length);
		bytes.putInt(msg.getUserId());
		bytes.putInt(encryptedMsg.length);
		bytes.put(encryptedMsg);
		bytes.putShort(msg.getCheckSum());
		return bytes.array();
	}
	
}

package lab4.core;

import java.nio.ByteBuffer;

import lab4.coreUtils.CheckSumCalculator;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
public class Message {

	private int userId;
	
	private String message;
	
	private short checkSum;
	
	public Message(int userId, String message) {
		this.userId = userId;
		this.message = message;
		ByteBuffer bytes = ByteBuffer.allocate(2 * Integer.BYTES + this.message.getBytes().length);
		bytes.putInt(this.userId);
		bytes.put(this.message.getBytes());
		this.checkSum = (short)CheckSumCalculator.getCRC16(bytes.array());
	}
	
}